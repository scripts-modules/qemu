
# ----------------------------------
# QEMU
# ----------------------------------

qemu_unix_monitor () {
    # Send command to monitor socket and remove monitor info (2 first lines)
    # QEMU X.Y.Z monitor - type 'help' for more information
    # (qemu) <command passed>

    local socket
    socket="$1"
    if [ -S "$socket" ]
    then
        echo "$2" | socat - UNIX-CONNECT:"${socket}" | grep -vE '^(\(qemu\)|QEMU)'
    else
        printf -- "[-] error %s is not a unix socket\n"
    fi
}
